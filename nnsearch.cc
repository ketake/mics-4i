#include <iostream>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>
#include <random>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <complex>
#include <chrono>
using namespace std;

typedef unsigned int uint;
typedef uint Item;
typedef unsigned long ulong;

typedef vector< vector<Item> > Database; //2 次元配列の型定義
Database db;

//ハッシュバケツ
typedef struct Bucket{
	ulong  hash_value; //後に使用する
 	vector<Item> Items;
} Bucket;


//ハッシュテーブル
typedef struct HashTable {
  	uint table_size; //テーブルサイズ
  	Bucket *buckets;//バケツの配列
	
	/*g(p)の計算に必要な情報*/
	uint elemkind; //要素の種類数
	uint tuple_num; //連結するハッシュ関数の数（ｃ）
	vector < vector<uint> > permutation; //tuple_num個のハッシュ案数
	/*2/17
 	vector < vector<uint> > permutaion(784);として宣言していたら下のシャッフル関数
	で4個のエラーが出たが、(784)を外したらエラーが全て消えた。
	*/
	uint *coeff; //F(p)を計算するときの係数を格納
} HashTable;

//ハッシュ値計算
uint minhash(vector<Item> &data,vector<uint> &permutation){
        uint hash=10000;
        uint i;
	for(i=0;i<data.size();i++){//配列辿る
                if(data[i]==1){//dataに1が立ってて、
                        if(hash>permutation[i])hash=permutation[i];//現在のハッシュより小さかったらhashを更新
                }
        }
        return hash;
}

//ハッシュテーブルへの挿入
//課題2-3から引数にpermutation追加
/*課題2-4
void insert(Item data, HashTable &table,vector<uint> &permutation){
	uint index = minhash(db[data],permutation);
	table.buckets[index].Items.push_back(data);
}*/

void insert(vector<Item> &p,Item dataID, HashTable &table){
	uint i=0,j=0;

	//1.mh(p)の計算
	vector<uint> mh(table.tuple_num);	
	for(i=0;i<table.tuple_num;i++){
		mh[i] = minhash(p,table.permutation[i]);
	}
	
	//2.g(p)の計算
	ulong g_p = 0;
	ulong poka;
	for(i=0;i<table.tuple_num;i++){
		poka = mh[i];
		for(j=i;j<table.tuple_num-1;j++){
			poka = poka*table.elemkind; 	
		}
		g_p += poka;
	}
	
	//3.F(g(p))の計算
	ulong F_g_p = 0;
	for(i=0;i<table.tuple_num;i++){
		F_g_p += mh[i]*table.coeff[i];
	}
	F_g_p = F_g_p % table.table_size;
	
	//4.tableへの登録
	//if(バケツが空)
	if(table.buckets[F_g_p].hash_value == 0xffffffffffffffff){
		table.buckets[F_g_p].hash_value = g_p;
		table.buckets[F_g_p].Items.push_back(dataID);
	}else{
		if(table.buckets[F_g_p].hash_value == g_p){
			table.buckets[F_g_p].Items.push_back(dataID);
		}else{
			i = F_g_p;
			i++;
			while(1){
				if(table.buckets[i].hash_value == 0xffffffffffffffff){
					table.buckets[F_g_p].hash_value = g_p;
					table.buckets[F_g_p].Items.push_back(dataID);
					break;
				}
				if(table.buckets[F_g_p].hash_value == g_p){
					table.buckets[F_g_p].Items.push_back(dataID);
					break;
				}
				i++;
			}
		}
	}	
}
/*
//ハッシュテーブルからの探索
bool search(Item query, HashTable &table){
\
    uint index = myhash(query,table.table_size);

    bool isfound=false;

    for(uint i=0;i<table.buckets[index].Items.size();i++) 
      if(table.buckets[index].Items[i]==query) isfound = true;

    return isfound;

}
*/
/*
//jaccardの近似値を計算する
double calc_kjaccrd(int k,vector<Item> &s1,vector<Item> &s2){
	int match = 0;
	vector<uint> permutation(784);
	iota(permutation.begin(),permutation.end(),0);
	for(int i=0;i<k;i++){
		// シャッフル
		std::random_device seed_gen;
		std::mt19937 engine(seed_gen());
		std::shuffle(permutation.begin(), permutation.end(), engine);
		//cout << permutation[0] << endl;//test
		if(minhash(s1,permutation) == minhash(s2,permutation) )match++;
	}

	return (double)match/(double)k;
}


//jaccardの真の値を計算する
double calc_sinjaccrd(vector<Item> &s1,vector<Item> &s2){
	uint i=0;
	int a = 0,b = 0, c = 0;
	for(i=0;i<s1.size();i++){
		if(s1[i]==1&&s2[i]==1)a++;
		else if(s1[i]==1)b++;
		else if(s2[i]==1)c++;
	}
	double jac = (double)a/(double)(a+b+c);
	return jac;
}
*/
int main(int argc, char* argv[]){
	/*******************ファイル読み込み******************/
	//ファイルパスが指定されていなかったら
	if(argc != 2){
		cout << "Error! This Program need filepath" << endl;
  	}
	
	//ファイルパスからオブジェくと作成
	ifstream ifs(argv[1]);
	//エラーの時
	if(!ifs){
		cout << "Error! File can not be opened." << endl;
		return 0;
	}

	/********************データベース登録******************/
	db.resize(10000);//test_imageから10行読み込む
	string str = "";
	int i=0,j=0,start;
	uint image=0;
	string scale;	
	//ファイルの中身を読み取って表示
	//下集合表現ではなく、ベクトル表現で格納する
	while(getline(ifs,str)){
		//cout << "画像1枚分:" << endl;
		for(i=1;i<(int)str.size()-1;i++){
			start = i;
			while(str[i]!=' '&&i<=(int)str.size()-1)i++;
			scale = str.substr(start,i-start);
			if(scale!="0")db[image].push_back(1);
			else db[image].push_back(0);
			//cout << db[image][j] << endl;//test
			j++;
		}
		if(i<(int)str.size()){
			scale = str.substr(i,1);
			if(scale!="0")db[image].push_back(1);
			else db[image].push_back(0);
			//cout << db[image][j] << endl;//test
		//cout << endl << "i=" << i << "j=" << j << endl;
		}
		image++;
		j=0;
	}
	/*****************************************************/
	
	/********************ハッシュテーブル登録*************/
 	/*
	HashTable tab;
	// ハッシュテーブルサイズ
	uint table_size=784;

	// ハッシュテーブル初期化
	tab.table_size = table_size;
	tab.buckets = (Bucket *) calloc(table_size, sizeof(Bucket));

	//ハッシュ関数役のpermutation生成
	vector<uint> permutation(784);
	iota(permutation.begin(),permutation.end(),0);
		// シャッフル
	std::random_device seed_gen;
	std::mt19937 engine(seed_gen());
	std::shuffle(permutation.begin(), permutation.end(), engine);
	//データのハッシュテーブルへの登録
	for(i=0;i<(int)image;i++){
		insert(i,tab,permutation);
	}
	*/
	/*****************************************************/
    	
	/********************ハッシュテーブルを登録*************/
 	int c = atoi(argv[2]);
 	int l = atoi(argv[3]);
	vector<HashTable> ltab(l);
	for(i=0;i<l;i++){
		HashTable tab;
		tab.table_size = 100000;
		tab.elemkind = 784;
		tab.tuple_num = c;
		/*tab.permutation = (uint**)calloc(tab.tuple_num,sizeof(uint));
		for(j=0;j<(int)tab.tuple_num;j++){
			tab.permutation[j] = (uint*)calloc(tab.elemkind,sizeof(uint));
		}*/
		/*
 
		2/3現状
		ここのprmutaionの中身をシャッフルするときの配列(vectorの扱いがわからない！！
　		他にも配列とvectorの渡し方(&や*など)の文法がわからない！！
		他の課題やらないといけないからとりあえず保留！
		とりあえずコンパイルしてみて！

		*/




		for(j=0;j<(int)tab.tuple_num;j++){
			//
			tab.permutation[j].resize(784);
			//ハッシュ関数役のpermutation生成
			iota(tab.permutation[j].begin(),tab.permutation[j].end(),0);
			// シャッフル
			std::random_device seed_gen;
			std::mt19937 engine(seed_gen());
			std::shuffle(tab.permutation[j].begin(), tab.permutation[j].end(), engine);
		}
	}
	// ハッシュテーブル初期化
	//tab.table_size = table_size;
	tab.buckets = (Bucket *) calloc(tab.table_size, sizeof(Bucket));
	//for(i=0;i<(int)image;i++){
	//	insert(i,tab,tab.permutation);
	//}
	/*****************************************************/

	/***********************探索**************************/
	/*vector<Item> query = db[0];//クエリ集合
	int zyouik[6] = {0,0,0,0,0,0};
	int zyouijac[6] = {0,0,0,0,0,0};
	uint mhquery = minhash(query,permutation);
	int seikaisu = 0;
	for(i=0;i<(int)tab.buckets[mhquery].Items.size();i++){
		double jac;
		jac = calc_sinjaccrd(db[tab.buckets[mhquery].Items[i]],query);
		if(jac == 1)seikaisu++;
		for(j=0;j<6;j++){
			if(jac >= zyouijac[j]){//上位に入るなら
				int poka = 4;
				while(j<=poka){//j位以下を最下位からずらす
					zyouik[poka+1] = zyouik[poka];
					zyouijac[poka+1] = zyouijac[poka];
					poka--;
				}
				zyouik[j] = i;
				zyouijac[j] = jac;
				break;
			}
		}
	}
	//この段階でzyouik[6]にデータベースの添字
	//zyouijacにそのjac係数が入っている
	
	cout << "計算回数 = " << tab.buckets[mhquery].Items.size() << endl;
	//cout << "正解率 = " << (double)seikaisu/6 << endl; << endl; << endl; << endl; << endl; << endl; << endl; << endl; << endl;*/
	/*****************************************************/
	

	/***********************動作確認**********************/
	/*for(i=0;i<784;i++){
		cout << "index=" << i <<": " ;
		for(j=0;j<(int)tab.buckets[i].Items.size();j++){
			cout <<tab.buckets[i].Items[j] << " ";
		}
		cout << endl;
	}*/
	/*****************************************************/
	return 0;

}
