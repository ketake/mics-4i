#include <iostream>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>
#include <random>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <complex>

using namespace std;

typedef unsigned int uint;
typedef uint Item;

typedef vector< vector<Item> > Database; //2 次元配列の型定義
Database db;


uint minhash(vector<Item> &data,vector<uint> &permutation){
        uint hash=10000;
        uint i;
        for(i=0;i<data.size();i++){//配列辿る
                if(data[i]==1){//dataに1が立ってて、
                        if(hash>permutation[i])hash=permutation[i];//現在のハッシュより小さかったらhashを更新
                }
        }
        return hash;
}

//jaccardの近似値を計算する
double calc_kjaccrd(int k,vector<Item> &s1,vector<Item> &s2){
	int match = 0;
	vector<uint> permutation(784);
	iota(permutation.begin(),permutation.end(),0);
	for(int i=0;i<k;i++){
		// シャッフル
		std::random_device seed_gen;
		std::mt19937 engine(seed_gen());
		std::shuffle(permutation.begin(), permutation.end(), engine);
		//cout << permutation[0] << endl;//test
		if(minhash(s1,permutation) == minhash(s2,permutation) )match++;
	}

	return (double)match/(double)k;
}
//jaccardの真の値を計算する
double calc_sinjaccrd(vector<Item> &s1,vector<Item> &s2){
	uint i=0;
	int a = 0,b = 0, c = 0;
	for(i=0;i<s1.size();i++){
		if(s1[i]==1&&s2[i]==1)a++;
		else if(s1[i]==1)b++;
		else if(s2[i]==1)c++;
	}
	double jac = (double)a/(double)(a+b+c);
	return jac;
}
int main(int argc, char* argv[]){
	//ファイルパスが指定されていなかったら
	if(argc != 2){
		cout << "Error! This Program need filepath" << endl;
  	}
	
	//ファイルパスからオブジェくと作成
	ifstream ifs(argv[1]);
	//エラーの時
	if(!ifs){
		cout << "Error! File can not be opened." << endl;
		return 0;
	}
	//k = atoi(argv[2]);//k個のハッシュ関数を生成する
	db.resize(10);//test_imageから10行読み込む
	string str = "";
	int i=0,j=0,image=0,start;
	string scale;	
	//ファイルの中身を読み取って表示
	//下集合表現ではなく、ベクトル表現で格納する
	while(getline(ifs,str)){
		//cout << "画像1枚分:" << endl;
		for(i=1;i<(int)str.size()-1;i++){
			start = i;
			while(str[i]!=' '&&i<=(int)str.size()-1)i++;
			scale = str.substr(start,i-start);
			if(scale!="0")db[image].push_back(1);
			else db[image].push_back(0);
			//cout << db[image][j] << endl;//test
			j++;
		}
		if(i<(int)str.size()){
			scale = str.substr(i,1);
			if(scale!="0")db[image].push_back(1);
			else db[image].push_back(0);
			//cout << db[image][j] << endl;//test
		//cout << endl << "i=" << i << "j=" << j << endl;
		}
		image++;
		j=0;
		if(image == 10)break;//課題2
	}	
	
	double heikin=0,kinzigosa;
	int k = 1;
	double heikinti[10000];
	for(k=1;k<=10000;k++){
		cout << "k = " << k << ":";
		for(i=0;i<9;i++){
			for(j=i+1;j<10;j++){
				//cout << "計算ペア:db[" << i << "],db[" << j <<"]" << endl;
				kinzigosa = abs(calc_sinjaccrd(db[i],db[j]) - calc_kjaccrd(k,db[i],db[j]));
				heikin += kinzigosa;
			}				
		}
		heikin = heikin/45.000;
		heikinti[k-1]=heikin;
		if(k==1){
			cout << "近似誤差の平均値=" << heikin << endl;
		}else{
			cout << "近似誤差の平均値=" << heikin << "、近似誤差の平均値の減少幅=" << heikinti[k-2] - heikinti[k-1] << endl;;
		}
	}
	//cout << "近似値=" << calc_kjaccrd(k,db[0],db[1]) << endl;
	//cout << "真の値=" << calc_sinjaccrd(db[0],db[1]) << endl;
    	return 0;

}
