#include <iostream>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>
#include <random>
#include <algorithm>

using namespace std;

typedef unsigned int uint;
typedef uint Item;

typedef vector< vector<Item> > Database; //2 次元配列の型定義
Database db;

int main(int argc, char* argv[]){
	vector<int> v = {1,2,3,4,5,6,7,8};
	vector<int> u = {1,2,3,4,5,6,7,8};
    	 std::random_device seed_gen;
                std::mt19937 engine(seed_gen());
                std::shuffle(v.begin(), v.end(), engine);	
                std::shuffle(u.begin(), u.end(), engine);
	for(int i=0;i<v.size();i++){
		cout << v[i] << endl;
		cout << u[i] << endl;
	}
	return 0;

}
