#include <iostream>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>
using namespace std;

typedef unsigned int uint;
typedef uint Item;

typedef vector< vector<Item> > Database; //2 次元配列の型定義
Database db;

/*
void fileopen(int argc, char* argv[]){
	//ファイルパスが指定されていなかったら
	if(argc != 2){
		cout << "Error! This Program need filepath" << endl;
  	}
	
	//ファイルパスからオブジェくと作成
	ifstream ifs(argv[1]);
	//エラーの時
	if(!ifs){
		cout << "Error! File can not be opened." << endl;
		//return 0;
	}
	
	string str = "";
	
	//ファイルの中身を読み取って表示
	while(getline(ifs,str)){
		cout << "ファイルの中身:" << str << endl;
	}
}*/


int main(int argc, char* argv[]){
	//ファイルパスが指定されていなかったら
	if(argc != 2){
		cout << "Error! This Program need filepath" << endl;
  	}
	
	//ファイルパスからオブジェくと作成
	ifstream ifs(argv[1]);
	//エラーの時
	if(!ifs){
		cout << "Error! File can not be opened." << endl;
		return 0;
	}
	db.resize(10000);
	string str = "";
	int i=0,j=0,image=0,start;
	string scale;	
	//ファイルの中身を読み取って表示
	while(getline(ifs,str)){
		//cout << "画像1枚分:" << endl;
		for(i=1;i<(int)str.size()-1;i++){
			start = i;
			while(str[i]!=' '&&i<=(int)str.size()-1)i++;
			scale = str.substr(start,i-start);
			if(scale!="0"){
				db[image].push_back(j);
				cout << j << endl;
			}
			//cout << scale << endl;//test
			j++;
		}
		if(i<(int)str.size()){
			scale = str.substr(i,1);
			if(scale!="0")db[image].push_back(j);
			//cout << endl << "i=" << i << "j=" << j << endl;
		}
		image++;
		j=0;
	}
	
    	return 0;

}
