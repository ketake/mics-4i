#include <iostream>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>
using namespace std;

typedef unsigned int uint;
typedef uint Item;

typedef vector< vector<Item> > Database; //2 次元配列の型定義
Database db;


uint minhash(vector<Item> &data,vector<uint> &permutation){
	uint hash=10000;
	uint i;
	for(i=0;i<data.size();i++){//配列辿る
		if(data[i]==1){//dataに1が立ってて、
			if(hash>permutation[i])hash=permutation[i];//現在のハッシュより小さかったらhashを更新
		}
	}
	return hash;
}

int main(int argc, char* argv[]){
	vector<uint> date = {1,0,0,0,1,1,0,0};
	vector<uint> permutation = {6,1,2,3,8,7,5,4};
	cout << "hash=" << minhash(date,permutation) << endl;
    	return 0;

}
