#include <iostream>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>
#include <random>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <complex>

using namespace std;

typedef unsigned int uint;
typedef uint Item;

typedef vector< vector<Item> > Database; //2 次元配列の型定義
Database db;

//ハッシュバケツ
typedef struct Bucket{
  //uint hash_value; //後に使用する
     vector<Item> Items;
} Bucket;


//ハッシュテーブル
typedef struct HashTable {
  uint table_size; //テーブルサイズ
  Bucket *buckets;//バケツの配列
} HashTable;

//ハッシュ値計算
uint minhash(vector<Item> &data,vector<uint> &permutation){
        uint hash=10000;
        uint i;

	for(i=0;i<data.size();i++){//配列辿る
                if(data[i]==1){//dataに1が立ってて、
                        if(hash>permutation[i])hash=permutation[i];//現在のハッシュより小さかったらhashを更新
                }
        }
        return hash;
}

//ハッシュテーブルへの挿入
//課題2-3から引数にpermutation追加
void insert(Item data, HashTable &table,vector<uint> &permutation){
	uint index = minhash(db[data],permutation);
	table.buckets[index].Items.push_back(data);
}

/*
//ハッシュテーブルからの探索
bool search(Item query, HashTable &table){

    uint index = myhash(query,table.table_size);

    bool isfound=false;

    for(uint i=0;i<table.buckets[index].Items.size();i++) 
      if(table.buckets[index].Items[i]==query) isfound = true;

    return isfound;

}
*/

//jaccardの近似値を計算する
double calc_kjaccrd(int k,vector<Item> &s1,vector<Item> &s2){
	int match = 0;
	vector<uint> permutation(784);
	iota(permutation.begin(),permutation.end(),0);
	for(int i=0;i<k;i++){
		// シャッフル
		std::random_device seed_gen;
		std::mt19937 engine(seed_gen());
		std::shuffle(permutation.begin(), permutation.end(), engine);
		//cout << permutation[0] << endl;//test
		if(minhash(s1,permutation) == minhash(s2,permutation) )match++;
	}

	return (double)match/(double)k;
}


//jaccardの真の値を計算する
double calc_sinjaccrd(vector<Item> &s1,vector<Item> &s2){
	uint i=0;
	int a = 0,b = 0, c = 0;
	for(i=0;i<s1.size();i++){
		if(s1[i]==1&&s2[i]==1)a++;
		else if(s1[i]==1)b++;
		else if(s2[i]==1)c++;
	}
	double jac = (double)a/(double)(a+b+c);
	return jac;
}

int main(int argc, char* argv[]){
	/*******************ファイル読み込み******************/
	//ファイルパスが指定されていなかったら
	if(argc != 2){
		cout << "Error! This Program need filepath" << endl;
  	}
	
	//ファイルパスからオブジェくと作成
	ifstream ifs(argv[1]);
	//エラーの時
	if(!ifs){
		cout << "Error! File can not be opened." << endl;
		return 0;
	}

	/********************データベース登録******************/
	db.resize(10000);//test_imageから10行読み込む
	string str = "";
	int i=0,j=0,start;
	uint image=0;
	string scale;	
	//ファイルの中身を読み取って表示
	//下集合表現ではなく、ベクトル表現で格納する
	while(getline(ifs,str)){
		//cout << "画像1枚分:" << endl;
		for(i=1;i<(int)str.size()-1;i++){
			start = i;
			while(str[i]!=' '&&i<=(int)str.size()-1)i++;
			scale = str.substr(start,i-start);
			if(scale!="0")db[image].push_back(1);
			else db[image].push_back(0);
			//cout << db[image][j] << endl;//test
			j++;
		}
		if(i<(int)str.size()){
			scale = str.substr(i,1);
			if(scale!="0")db[image].push_back(1);
			else db[image].push_back(0);
			//cout << db[image][j] << endl;//test
		//cout << endl << "i=" << i << "j=" << j << endl;
		}
		image++;
		j=0;
	}
	/*****************************************************/
	
	/********************ハッシュテーブル登録*************/
 	HashTable tab;
	// ハッシュテーブルサイズ
	uint table_size=784;

	// ハッシュテーブル初期化
	tab.table_size = table_size;
	tab.buckets = (Bucket *) calloc(table_size, sizeof(Bucket));

	//ハッシュ関数役のpermutation生成
	vector<uint> permutation(784);
	iota(permutation.begin(),permutation.end(),0);
		// シャッフル
	std::random_device seed_gen;
	std::mt19937 engine(seed_gen());
	std::shuffle(permutation.begin(), permutation.end(), engine);
	//データのハッシュテーブルへの登録
	for(i=0;i<(int)image;i++){
		insert(i,tab,permutation);
	}
	/*****************************************************/
    	
	/***********************探索**************************/
	vector<Item> query = db[0];//クエリ集合
	int zyouik[6] = {0,0,0,0,0,0};
	int zyouijac[6] = {0,0,0,0,0,0};
	uint mhquery = minhash(query,permutation);
	int seikaisu = 0;
	for(i=0;i<(int)tab.buckets[mhquery].Items.size();i++){
		double jac;
		jac = calc_sinjaccrd(db[tab.buckets[mhquery].Items[i]],query);
		if(jac == 1)seikaisu++;
		for(j=0;j<6;j++){
			if(jac >= zyouijac[j]){//上位に入るなら
				int poka = 4;
				while(j<=poka){//j位以下を最下位からずらす
					zyouik[poka+1] = zyouik[poka];
					zyouijac[poka+1] = zyouijac[poka];
					poka--;
				}
				zyouik[j] = i;
				zyouijac[j] = jac;
				break;
			}
		}
	}
	//この段階でzyouik[6]にデータベースの添字
	//zyouijacにそのjac係数が入っている
	
	cout << "計算回数 = " << tab.buckets[mhquery].Items.size() << endl;
	//cout << "正解率 = " << (double)seikaisu/6 << endl; << endl; << endl; << endl; << endl; << endl; << endl; << endl; << endl;;	
	/*****************************************************/
	

	/***********************動作確認**********************/
	/*for(i=0;i<784;i++){
		cout << "index=" << i <<": " ;
		for(j=0;j<(int)tab.buckets[i].Items.size();j++){
			cout <<tab.buckets[i].Items[j] << " ";
		}
		cout << endl;
	}*/
	/*****************************************************/
	return 0;

}
