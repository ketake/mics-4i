#include <iostream>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>
#include <random>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <complex>

using namespace std;

typedef unsigned int uint;
typedef uint Item;

typedef vector< vector<Item> > Database; //2 次元配列の型定義
Database db;

//ハッシュバケツ
typedef struct Bucket{
  //uint hash_value; //後に使用する
     vector<Item> Items;
} Bucket;


//ハッシュテーブル
typedef struct HashTable {
  uint table_size; //テーブルサイズ
  Bucket *buckets;//バケツの配列
} HashTable;

//ハッシュ値計算
uint minhash(vector<Item> &data,vector<uint> &permutation){
        uint hash=10000;
        uint i;

	for(i=0;i<data.size();i++){//配列辿る
                if(data[i]==1){//dataに1が立ってて、
                        if(hash>permutation[i])hash=permutation[i];//現在のハッシュより小さかったらhashを更新
                }
        }
        return hash;
}

//ハッシュテーブルへの挿入
//課題2-3から引数にpermutation追加
void insert(Item data, HashTable &table,vector<uint> &permutation){
	uint index = minhash(db[data],permutation);
	table.buckets[index].Items.push_back(data);
}

/*
//ハッシュテーブルからの探索
bool search(Item query, HashTable &table){

    uint index = myhash(query,table.table_size);

    bool isfound=false;

    for(uint i=0;i<table.buckets[index].Items.size();i++) 
      if(table.buckets[index].Items[i]==query) isfound = true;

    return isfound;

}
*/

//jaccardの近似値を計算する
double calc_kjaccrd(int k,vector<Item> &s1,vector<Item> &s2){
	int match = 0;
	vector<uint> permutation(784);
	iota(permutation.begin(),permutation.end(),0);
	for(int i=0;i<k;i++){
		// シャッフル
		std::random_device seed_gen;
		std::mt19937 engine(seed_gen());
		std::shuffle(permutation.begin(), permutation.end(), engine);
		//cout << permutation[0] << endl;//test
		if(minhash(s1,permutation) == minhash(s2,permutation) )match++;
	}

	return (double)match/(double)k;
}


int main(int argc, char* argv[]){
	/*******************ファイル読み込み******************/
	//ファイルパスが指定されていなかったら
	if(argc != 2){
		cout << "Error! This Program need filepath" << endl;
  	}
	
	//ファイルパスからオブジェくと作成
	ifstream ifs(argv[1]);
	//エラーの時
	if(!ifs){
		cout << "Error! File can not be opened." << endl;
		return 0;
	}

	/********************データベース登録******************/
	db.resize(10000);//test_imageから10行読み込む
	string str = "";
	int i=0,j=0,start;
	uint image=0;
	string scale;	
	//ファイルの中身を読み取って表示
	//下集合表現ではなく、ベクトル表現で格納する
	while(getline(ifs,str)){
		//cout << "画像1枚分:" << endl;
		for(i=1;i<(int)str.size()-1;i++){
			start = i;
			while(str[i]!=' '&&i<=(int)str.size()-1)i++;
			scale = str.substr(start,i-start);
			if(scale!="0")db[image].push_back(1);
			else db[image].push_back(0);
			//cout << db[image][j] << endl;//test
			j++;
		}
		if(i<(int)str.size()){
			scale = str.substr(i,1);
			if(scale!="0")db[image].push_back(1);
			else db[image].push_back(0);
			//cout << db[image][j] << endl;//test
		//cout << endl << "i=" << i << "j=" << j << endl;
		}
		image++;
		j=0;
	}
	/*****************************************************/
	
	/********************ハッシュテーブル登録*************/
 	HashTable tab;
	// ハッシュテーブルサイズ
	uint table_size=784;

	// ハッシュテーブル初期化
	tab.table_size = table_size;
	tab.buckets = (Bucket *) calloc(table_size, sizeof(Bucket));

	//ハッシュ関数役のpermutation生成
	vector<uint> permutation(784);
	iota(permutation.begin(),permutation.end(),0);
		// シャッフル
	std::random_device seed_gen;
	std::mt19937 engine(seed_gen());
	std::shuffle(permutation.begin(), permutation.end(), engine);
	//データのハッシュテーブルへの登録
	vector<uint> test(784);
	for(i=0;i<(int)image;i++){
		for(j=0;j<784;j++){
			test[j] = db[i][permutation[j]];
		}
		for(int k=0;k<784;k++){
			cout << db[i][k] << " ";
		}
		cout << endl;	
		for(int k=0;k<784;k++){
			cout << test[k] << " ";
		}
		cout << endl;
	}
	/*****************************************************/
    	
	return 0;

}
