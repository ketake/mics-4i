#include <iostream>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>
using namespace std;

typedef unsigned int uint;
typedef uint Item;

typedef vector< vector<Item> > Database; //2 次元配列の型定義
Database db;

double calc_jaccrd(vector<Item> &s1,vector<Item> &s2){
	uint jac=0;//総アイテム数
	uint dupitem=0;//重複アイテム数
	uint i=0,j=0;//s1とs2を辿るための添字
	while(1){
		//s1を全てたどり終わった時の処理
		if(i==s1.size()){
			jac += s2.size()-j;
			break;
		}	
		if(j==s2.size()){
			jac += s1.size()-i;
			break;
		}	
		if(s1[i]==s2[j]){
			jac++;
			dupitem++;
			i++;j++;
		}else if(s1[i]>s2[j]){
			jac++;
			j++;
		}else{
			jac++;
			i++;
		}
	}
	//cout << "dupitem=" <<  dupitem << " jac="  <<jac << endl;	
	return (double)dupitem/(double)jac;
}


int main(int argc, char* argv[]){
	//ファイルパスが指定されていなかったら
	if(argc != 2){
		cout << "Error! This Program need filepath" << endl;
  	}
	
	//ファイルパスからオブジェくと作成
	ifstream ifs(argv[1]);
	//エラーの時
	if(!ifs){
		cout << "Error! File can not be opened." << endl;
		return 0;
	}

	db.resize(10000);//test_imageから10行読み込む
	string str = "";
	int i=0,j=0,image=0,start;
	string scale;	
	//ファイルの中身を読み取って表示
	while(getline(ifs,str)){
		//cout << "画像1枚分:" << endl;
		for(i=1;i<(int)str.size()-1;i++){
			start = i;
			while(str[i]!=' '&&i<=(int)str.size()-1)i++;
			scale = str.substr(start,i-start);
			if(scale!="0")db[image].push_back(j);
			//cout << scale << endl;//test
			j++;
		}
		if(i<(int)str.size()){
			scale = str.substr(i,1);
			if(scale!="0")db[image].push_back(j);
		//cout << endl << "i=" << i << "j=" << j << endl;
		}
		image++;
		j=0;
	}	
	
	double sim;
	for(i=0;i<10000;i++){
		for(j=i+1;j<10000;j++){
			sim = calc_jaccrd(db[i],db[j]);	
			//cout << sim << endl;
		}
	}
    	return 0;

}
